#!/usr/bin/perl
#by nu11secur1ty@V.V
use strict;
use warnings;
use DBI;
use Term::ANSIColor;


print colored ("WARNING:", 'bold red'),"";
print colored (" You have to modify the program, what database you want to use!",'green'),"\n";
print colored ("Perl MySQL - Create Tables",'yellow'),"\n";
 
# MySQL database configurations
my $dsn = "DBI:mysql:_your_db_";           # DBI:mysql:your_database
my $username = "_your_user_";              # your username
my $password = "_your_password_";          # your password
 
# connect to MySQL database
my %attr = (PrintError=>0, RaiseError=>1);
      my $dbh = DBI->connect($dsn,$username,$password, \%attr);

# create table statements
my @ddl =     (
# create test table
 "CREATE TABLE test1 (
 tag_id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
 tag varchar(255) NOT NULL
         ) ENGINE=InnoDB;",
# create test2 table
        "CREATE TABLE test2 (
   link_id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
   title varchar(255) NOT NULL,
   url varchar(255) NOT NULL,
   target varchar(45) NOT NULL
 ) ENGINE=InnoDB;",

# create test3 table
        "CREATE TABLE test3 (
   link_id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
   title varchar(255) NOT NULL,
   url varchar(255) NOT NULL,
   target varchar(45) NOT NULL
 ) ENGINE=InnoDB;",

# create test3 table
# "CREATE TABLE test3 (
#  link_id int(11) NOT NULL,
#   tag_id int(11) NOT NULL,
#   PRIMARY KEY (link_id,tag_id),
#   KEY fk_link_idx (link_id),
#  KEY fk_tag_idx (tag_id),
#   CONSTRAINT fk_tag FOREIGN KEY (tag_id) 
#      REFERENCES tags (tag_id),
#   CONSTRAINT fk_link FOREIGN KEY (link_id) 
# REFERENCES links (link_id) 
# ) ENGINE=InnoDB"
        );
 
# execute all create table statements	       
for my $sql(@ddl){
       $dbh->do($sql);
}        
 
print "All tables created successfully!\n";
 
# disconnect from the MySQL database
      $dbh->disconnect();


      exit 0;



