#!/usr/bin/perl
#by nu11secur1ty@V.V
use strict;
use warnings;
use DBI;
use Term::ANSIColor;


print colored ("WARNING:", 'bold red'),"";
print colored (" You have to modify the program, what database you want to use!",'green'),"\n";
print colored ("Perl MySQL - Drop Tables",'yellow'),"\n";


my $hostname = '_your_host_';         # your host
my $database = '_your_db_';           # your database
my $username = '_your_user_';         # your user
my $password = '_your_passwoed_';     # your password

   my $dbh = DBI->connect("dbi:mysql:${database}:$hostname",
      $username, $password) or die "Error: $DBI::errstr\n";

my $sth = $dbh->prepare("SHOW TABLES");
   $sth->execute or die "SQL Error: $DBI::errstr\n";
my $i = 0;
my @all_tables = ();
while(my $table = $sth->fetchrow_array)
{
  $i++;
  print "table $i: $table\n";
  push @all_tables, $table;
}
my $total_table_count = $i;

print "Enter string or regex to match tables to "
  . "delete (won't delete yet) or (press enter to close): ";
my $regex = <STDIN>;
chomp $regex;

$i = 0;
my @matching_tables = ();
foreach my $table (@all_tables){
  if($table =~ /$regex/i){
    $i++;
    print "matching table $i: $table\n";
    push @matching_tables, $table;
  }
}
my $matching_table_count = $i;

if($matching_table_count){
  print "$matching_table_count out of $total_table_count "
    . "tables match, and will be deleted.\n";
  print "Delete tables now? [y/n] ";
  my $decision = <STDIN>;
  chomp $decision;

  $i = 0;
  if($decision =~ /y/i){
    foreach my $table (@matching_tables)
    {
      $i++;
      print "deleting table $i: $table\n";
      my $sth = $dbh->prepare("DROP TABLE $table");
      $sth->execute or die "SQL Error: $DBI::errstr\n";
    }
  }else{
    print "Not deleting any tables.\n";
  }
    }else{
  print "No matching tables.\n";
}
  exit 0;

